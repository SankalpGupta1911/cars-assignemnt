function BMWAndAudi(invent){
    const List = [];
    for (let index=0; index<invent.length; index++){
        if(!invent[index].hasOwnProperty('car_year'))  {
            continue;
        }
        if (invent[index].car_make=="Audi" || invent[index].car_make=="BMW"){
            List.push(invent[index]);
        }
    }
    console.log(JSON.stringify(List));
}

module.exports = BMWAndAudi;