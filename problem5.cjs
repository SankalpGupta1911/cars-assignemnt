function oldCarList(invent,year){
    const List = [];
    for (let index=0; index<invent.length; index++){
        if(!invent[index].hasOwnProperty('car_year'))  {
            continue;
        }
        if (invent[index].car_year<2000){
            List.push(invent[index].car_year);
        }
    }
    console.log("The number of Cars older than the year 2000: " + List.length + "\nThe list of cars older then the year 2000: " + List);
} 

module.exports = oldCarList;