const idInfo = require("./problem1.cjs");
const invent = require("./cars.cjs");

let id = 24;

const res = idInfo(invent,id);
if (res){
    console.log("Car " + res.id + " is a " + res.car_year + " " + res.car_make + " " + res.car_model);
}
else{
    return [];
}