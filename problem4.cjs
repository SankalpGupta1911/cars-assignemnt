function yearList(invent){
    const List = [];
    for (let index=0; index<invent.length; index++){
        if(!invent[index].hasOwnProperty('car_year'))  {
            continue;
        }
        List.push(invent[index].car_year);
    }
    console.log(List);
}

module.exports = yearList;