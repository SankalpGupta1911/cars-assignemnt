function idInfo(inventory,id){
    if(id == undefined || typeof id != 'number' || inventory == undefined || !Array.isArray(inventory)) {
        return [];
    }
    for (let index=0; index<inventory.length; index++) {
        if (inventory[index].id == id){
           return inventory[index];
        }
    }
}

module.exports = idInfo;